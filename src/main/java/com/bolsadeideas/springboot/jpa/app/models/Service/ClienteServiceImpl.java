package com.bolsadeideas.springboot.jpa.app.models.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.jpa.app.models.dao.IClienteDao;
import com.bolsadeideas.springboot.jpa.app.models.entity.Cliente;

@Service
public class ClienteServiceImpl implements IClienteService {

	@Autowired
	IClienteDao clienteDao;
	

	@Transactional(readOnly = true)
	@Override
	public List<Cliente> findAll() {
		return clienteDao.findAll();
	}

	@Transactional
	@Override
	public void save(Cliente cliente) {
		clienteDao.save(cliente);
	}

	@Transactional
	@Override
	public Cliente findOne(Integer id) {
		return clienteDao.findOne(id);
	}

	@Transactional
	@Override
	public void remove(Integer id) {
		clienteDao.remove(id);
		
	}

	@Override
	public Page<Cliente> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

}
