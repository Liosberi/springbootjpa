package com.bolsadeideas.springboot.jpa.app.utils.paginador;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;

public class PageRender<T> {

	private String url;
	private Page<T> page;

	private int elementosPorPagina;
	private int paginaActual;
	private int totalPaginas;
	private List<PageItem> pageItems;

	public PageRender(String url, Page<T> page) {
		super();
		this.url = url;
		this.page = page;
		this.pageItems=new ArrayList<PageItem>();
		elementosPorPagina = page.getSize();
		paginaActual = page.getNumber()+1;
		totalPaginas = page.getTotalPages();
		int hasta = elementosPorPagina;
		int desde = 0;
		if (totalPaginas <= elementosPorPagina) {
			desde = 1;
		} else if (paginaActual <= elementosPorPagina/2) {
			desde = paginaActual;
		} else if (paginaActual >= totalPaginas - elementosPorPagina / 2) {
			desde = totalPaginas - elementosPorPagina+1;
		}else {
			desde=paginaActual - elementosPorPagina / 2;
		}
		for (int i = 0; i < hasta; i++) {
			pageItems.add(new PageItem(desde + i, desde+i == paginaActual));
		}

	}

	public int getPaginaActual() {
		return paginaActual;
	}

	public int getTotalPaginas() {
		return totalPaginas;
	}

	public List<PageItem> getPageItems() {
		return pageItems;
	}

	public String getUrl() {
		return url;
	}

	public Page<T> getPage() {
		return page;
	}

	public boolean isFirst() {
		return page.isFirst();
	}

	public boolean isHasNext() {
		return page.hasNext();
	}

	public boolean isHasPrevious() {
		return page.hasPrevious();
	}

	public boolean isLast() {
		return page.isLast();
	}
}
