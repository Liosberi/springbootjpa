package com.bolsadeideas.springboot.jpa.app.models.dao;

import java.util.List;

import com.bolsadeideas.springboot.jpa.app.models.entity.Cliente;

public interface IClienteDao {

	public List<Cliente> findAll();
	
	public void save(Cliente cliente);
	
	public Cliente findOne(Integer id);
	
	public void remove(Integer id);

}
