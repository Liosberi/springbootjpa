package com.bolsadeideas.springboot.jpa.app.models.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.jpa.app.models.dao.IClienteDao;
import com.bolsadeideas.springboot.jpa.app.models.dao.IClienteDao2;
import com.bolsadeideas.springboot.jpa.app.models.entity.Cliente;

@Service("ClientesAsCrudRepo")
public class ClienteServiceImpl2 implements IClienteService {

	@Autowired
	IClienteDao2 clienteDao;
	

	@Transactional(readOnly = true)
	@Override
	public List<Cliente> findAll() {
		return (List<Cliente>)clienteDao.findAll();
	}

	@Transactional
	@Override
	public void save(Cliente cliente) {
		clienteDao.save(cliente);
	}

	@Transactional
	@Override
	public Cliente findOne(Integer id) {
		return clienteDao.findById(id).orElse(null);
	}

	@Transactional
	@Override
	public void remove(Integer id) {
		clienteDao.deleteById(id);
		
	}

	@Transactional
	@Override
	public Page<Cliente> findAll(Pageable pageable) {
		return clienteDao.findAll(pageable);
	}

}
