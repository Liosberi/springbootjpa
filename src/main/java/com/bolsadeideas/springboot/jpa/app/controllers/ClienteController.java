package com.bolsadeideas.springboot.jpa.app.controllers;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.jpa.app.models.Service.IClienteService;
import com.bolsadeideas.springboot.jpa.app.models.entity.Cliente;
import com.bolsadeideas.springboot.jpa.app.utils.paginador.PageRender;

@Controller
@SessionAttributes("cliente")
public class ClienteController {
	
	@Autowired
	@Qualifier("ClientesAsCrudRepo")
	private IClienteService clienteService;
	
	@RequestMapping(value = "/listar",method = RequestMethod.GET)
	public String listar(Model model, @RequestParam(name="page", defaultValue = "0") int page) {
		Pageable pageRequest= PageRequest.of(page, 3);
		Page<Cliente> clientes=clienteService.findAll(pageRequest);
		PageRender<Cliente> pageRender=new PageRender<>("/listar", clientes);
		model.addAttribute("titulo", "Lista de usuarios");
		model.addAttribute("clientes",clientes);
		model.addAttribute("page", pageRender);
		return "listar";
	}
	
	@RequestMapping(value="/form")
	public String crear(Map<String, Object> map) {
		Cliente cliente = new Cliente();
		map.put("titulo", "Agregr usuario");
		map.put("cliente", cliente);
		return "form";
	}
	
	@RequestMapping(value = "/form" , method = RequestMethod.POST)
	public String guardar(@Valid @ModelAttribute("cliente") Cliente cliente, BindingResult result, Model model,SessionStatus status,RedirectAttributes flash) {
		if (result.hasErrors()) {
			model.addAttribute("titulo", "Lista de usuarios");
			return "form";
		}
		String messaFlash= (cliente.getId()!= null)? "Usuario editado correctamente" :"Usuario creado correctamente";
		
		clienteService.save(cliente);		
		status.isComplete();
		flash.addFlashAttribute("succes",messaFlash);
		return "redirect:/listar";
	}
	
	@RequestMapping(value = "/form/{id}", method = RequestMethod.GET)
	public String editar(@PathVariable("id") Integer id, Model model, RedirectAttributes flush) {
		Cliente newCliente= new Cliente();
		
		if(id >0) {
			newCliente= clienteService.findOne(id);
			if(newCliente != null) {
				model.addAttribute("cliente", newCliente);;
				model.addAttribute("titulo", "Editar usuarios");
				flush.addFlashAttribute("succes","Cliente editado correctamente");
				return "form";
			}
			else {
				flush.addFlashAttribute("danger","Id del cliente no puede ser nulo");
				return "redirect:/listar";
			}
			
		}
		flush.addFlashAttribute("danger","Id del cliente no puede ser menor o igual a 0");
		return "redirect:/listar";
		
		
		
	}
	
	@RequestMapping(value = "/eliminar/{id}")
	public String eliminar(@PathVariable("id") Integer id, Model model) {
		clienteService.remove(id);
		model.addAttribute("titulo", "Editar usuarios");	
		return"redirect:/listar";
	}
}
